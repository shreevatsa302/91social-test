import 'bootstrap/dist/css/bootstrap.min.css';
import Login from "./components/login.component";
import SignUp from "./components/signup.component";
import NavBar from './components/navbar.component';
import Blogs from './components/blogs/blogs';
import './App.css';

import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import React from "react";
import DashBoard from './components/dashboard.component';

function App() {


 


  return (
    <Router >
    <div className="App">

      <NavBar/>

      <Switch>
                            <Route exact path="/" component={DashBoard}/>
                            <Route path="/login" component={Login} />
                            <Route path="/register" component={SignUp} />
                            <Route path="/blogs/:id" component={Blogs} />

                            <Redirect from="*" to="/" />
      </Switch>

     
     
    </div>
    </Router>
    
  );
}

export default App;
