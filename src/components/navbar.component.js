import logo from '../logo.png';
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { logout } from '../redux/AuthProvider';

const NavBar = () => {


  const {auth,role} = useSelector((state)=>state.auth);



    return ( 
        <nav className="navbar navbar-expand-lg navbar-light fixed-top" style={{height:"60px"}}>
        <div className="container">
          <Link className="navbar-brand" to={"/sign-in"}>
            <img className="logo" src={logo} alt=""/>
          </Link>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
           {auth ? AuthenticatedNav(role) : NotAuthenicatedNav()}
          </div>
        </div>
      </nav>
     );
}

const NotAuthenicatedNav = () => {
  return ( 
    <ul className="navbar-nav ml-auto">


    <li className="nav-item">
      <Link className="nav-link" to={"/sign-in"}>Login</Link>
    </li>
    <li className="nav-item">
      <Link className="nav-link" to={"/register"}>SignUp</Link>
    </li>
  </ul>
   );
}
 
const AuthenticatedNav = (role) =>{
  const dispatch = useDispatch();

  return (
    <ul className="navbar-nav ml-auto">
      

    <li className="nav-item">
      <div className="nav-link" onClick={()=>dispatch(logout())}>{role.toUpperCase()} LOGOUT</div>
    </li>
  </ul>
  );
} 
export default NavBar;