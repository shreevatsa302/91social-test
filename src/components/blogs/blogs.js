import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import useFetch from "../../hooks/useFetch";
import '../../home.css'
import Moment from 'react-moment'

const Blogs = (props) => {
    Moment.globalFormat = 'D MMM YYYY';
    const {id} = props.match.params;

   const {data : blog,error,ispending} = useFetch('https://api.spacexdata.com/v4/launches/'+id.toString());



    const {auth} = useSelector((state)=>state.auth);

   
    if (!auth) {
        return <Redirect to='/login' />
      }
    
    return ( 
        <div className="dash">
            {ispending && <div>Loading</div>}
            {error && <div>{error}</div>}
            {blog && (
                <article className="blog">
<div >
  <div >
  <img src = {blog.links.patch.large} alt="no image" style={{maxHeight:"30%",maxWidth:"30%"}}/>


  </div>
  <div  style={{justifyContent:"center",textAlign:"center"}}>
    <h1>{blog.name} Launched</h1>
    <h5> On <Moment unix>{blog.static_fire_date_unix}</Moment></h5>
    <p style={{textAlign: "justify",
  textJustify: "inter-word"}}>{blog.details}</p>

  <iframe src={"https://www.youtube.com/embed/"+blog.youtube_id} width="100%" height="50%" title="youtube"></iframe>
  <div style={{height:"30%"}}></div>
  </div>

                    </div>


                  
                
                </article>)}
            </div>)
}
 
export default Blogs;