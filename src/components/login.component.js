import { useState } from "react";
import { useSelector,useDispatch } from "react-redux";
import LoginHook from "../hooks/loginHook";
import { Redirect } from "react-router-dom";
import { connect } from 'react-redux'

  const Login = () => {

    const [error,handleError] = useState(undefined);
    const [clicked,handleButton] = useState(false);

const {auth} = useSelector((state) => state.auth);
const dispatch = useDispatch();



console.log("Auth",auth)
if (auth) {
    return <Redirect to='/' />
  }


const handleClick = async (event)=>{
    event.preventDefault();
    handleButton(true);

    let email = event.target.email.value;
    let password = event.target.password.value;
    const data = {
     email, password
    };
    let res = await LoginHook(data,dispatch);
    if(res.message==="SUCCESS"){
    }
    else{
        handleError(res.message);
    }
}



    return (
        <div className="auth-wrapper">
        <div className="auth-inner">


                       
          
          
       
        <form onSubmit={handleClick}>


            <h3>Log In</h3>

            <div className="form-group">
                <label>Email address</label>
                <input type="email" className="form-control" placeholder="Enter email" name="email" required/>
            </div>

            <div className="form-group">
                <label>Password</label>
                <input type="password" className="form-control" placeholder="Enter password" name="password" required/>
            </div>

            <div className="form-group">
                <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="customCheck1" />
                    <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                </div>
            </div>
            {clicked && error ? <div ><p style={{color:"red"}}>{error}</p></div> : <div></div>}

            <button type="submit" className="btn btn-primary btn-block" >{auth ?"Logout" :"Login" }</button>
            <p className="forgot-password text-right">
                Don't have an account? <a href="/register">SIGN UP</a>
            </p>
        </form>
        </div>
      </div>

    );
  }
   
  export default connect()(Login)


