import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import useFetch from "../hooks/useFetch";
import '../home.css'
import Moment from 'react-moment'
import Button from 'react-bootstrap/Button';
import { Card, Col ,Row} from 'react-bootstrap';

const DashBoard = () => {
    Moment.globalFormat = 'D MMM YYYY';

   const {data : blog,error,ispending} = useFetch('https://api.spacexdata.com/v4/launches/latest');

   const {data : allblog} = useFetch('https://api.spacexdata.com/v4/launches');


    const {auth} = useSelector((state)=>state.auth);

   
    if (!auth) {
        return <Redirect to='/login' />
      }
    
    return ( 
        <div className="dash">
            {ispending && <div>Loading</div>}
            {error && <div>{error}</div>}
            {blog && (
                <article className="article">
<div className="row">
  <div className="column">
  <img src = {blog.links.patch.large} style={{width:"50%"}}/>


  </div>
  <div className="column" style={{justifyContent:"center",textAlign:"center"}}>
    <h1>{blog.name} Launched</h1>
    <h5> On <Moment unix>{blog.static_fire_date_unix}</Moment></h5>
    <p style={{textAlign: "justify",
  textJustify: "inter-word"}}>{blog.details}</p>
    <Button variant="light" href={"/blogs/"+blog.id}>Learn more</Button> 

  </div>

                    </div>


                  
                
                </article>
            )}
<div >
<h2 className="launch">All Launches</h2>

<Row>

            {allblog && (
  Object.entries(allblog).map((item,index) => {

    console.log(allblog[index]);

    if(allblog[index].links && allblog[index].links.patch && allblog[index].links.patch.large){
        return(
            <Col md="6" xl="3" sm="12"> 
            <div className="cards">
            <Card key={index} className="box" style={{ width: '18rem'}}>
            <Card.Img variant="top" src={allblog[index].links.patch.large} placeholder={blog.links.patch.large} style={{paddingLeft:"20px",paddingRight:"20px",paddingTop:"30px",paddingBottom:"20px"}}/>
            <Card.Body>
              <Card.Title>{allblog[index].name}</Card.Title>
              <Card.Text style={{height:"50px",overflow:"hidden"}}>
               {allblog[index].details}
              </Card.Text>
              <Button variant="primary" href={"/blogs/"+allblog[index].id}>Learn more</Button>
            </Card.Body>
          </Card>
          </div>
          </Col>

        );
            
    }
    

})
)}
</Row>

</div>
        </div>
     );
}
 
export default DashBoard;