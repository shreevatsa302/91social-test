import { useState } from "react";
import {  useSelector } from "react-redux";
import { Redirect, useHistory } from "react-router-dom";

import registerHook from "../hooks/registerHook";


const SignUp = () => {
    const [selectValue, handleChange] = useState('admin');
    const [clicked,handleButton] = useState(false);
    const [error,handleError] = useState(undefined);

    const history = useHistory();

const {auth} = useSelector((state) => state.auth);

console.log("error",auth);


if (auth) {
    return <Redirect to='/' />
  }
const handleClick = async (event)=>{

    event.preventDefault();
    let name = event.target.name.value;
    let email = event.target.email.value;
    let password = event.target.password.value;
    let role = event.target.role.value;
    const data = {
      name, email, password,role
    };

    handleButton(true);

   let res = await registerHook(data);

   console.log(res);

   if(res.registered){
       handleError(undefined);
       console.log("go to login");
       history.push("login");

   }
   else{
       
       handleError(res.error);
   }


    
}

    return ( 
        <div className="auth-wrapper">
        <div className="auth-inner">
        <form onSubmit={handleClick} method="post">
                <h3>Sign Up</h3>

                <div className="form-group">
                    <label>Full name</label>
                    <input type="text" name="name" className="form-control" placeholder="Full name" required/>
                </div>

                <div className="form-group">
                    <label>Email address</label>
                    <input type="email" name="email" className="form-control" placeholder="Enter email" required/>
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" name="password" className="form-control" placeholder="Enter password" required/>
                </div>

                <div className="form-group">
                <label>Role</label>

                <select value={selectValue} name="role" className="form-control" onChange={(e)=>handleChange(e.target.value)}>
  <option value="admin">ADMIN</option>
  <option value="user">USER</option>
</select>          
                </div>


                {clicked && error ? <div ><p style={{color:"red"}}>{error}</p></div> : <div></div>}


                <button type="submit" className="btn btn-primary btn-block">Sign Up</button>
                <p className="forgot-password text-right">
                    Already registered <a href="/">sign in?</a>
                </p>
            </form>
            </div>
      </div>
     );
}
 
export default SignUp;

