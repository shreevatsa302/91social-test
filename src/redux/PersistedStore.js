import {
  configureStore,
  combineReducers,
} from "@reduxjs/toolkit";

// import { createStateSyncMiddleware, initStateWithPrevTab } from 'redux-state-sync';



  import AuthProvider from "./AuthProvider";
  
  const reducer = combineReducers({
    auth: AuthProvider
  });
  
  const LOCAL_STORAGE_NAME = "DB";
  
  class PersistedStore {
  
    static DefaultStore = null;
  
    static getDefaultStore() {
      if (PersistedStore.DefaultStore === null) {
        PersistedStore.DefaultStore = new PersistedStore();
      }
  
      return PersistedStore.DefaultStore;
    }
  
    _store = null;
  
    constructor() {
      this.initStore()
    }
  
    initStore() {

    //   const config = {
    //     blacklist: [],
    // };
    // const middlewares = [createStateSyncMiddleware(config)];

      this._store = configureStore({
        reducer,
        // middleware:middlewares,
        preloadedState: PersistedStore.loadState()
      });

      // initStateWithPrevTab(this._store);

      this._store.subscribe(() => {
        PersistedStore.saveState(this._store.getState());
      });
    }
  
    get store() {
      return this._store;
    }
  
    static loadState() {
      try {
        let serializedState = localStorage.getItem(LOCAL_STORAGE_NAME);
  
        if (serializedState === null) {
          return PersistedStore.initialState();
        }
  
        return JSON.parse(serializedState);
      } catch (err) {
        return PersistedStore.initialState();
      }
    }
  
    static saveState(state) {
      try {
        let serializedState = JSON.stringify(state);
        localStorage.setItem(LOCAL_STORAGE_NAME, serializedState);
      } catch (err) {
        console.log(err);
      }
    }
  
    static initialState() {
      return {};
    }
  }
  
  export default PersistedStore;