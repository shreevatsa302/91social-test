import { createSlice } from "@reduxjs/toolkit";


const AuthProvider = createSlice({
  name: "auth",
  initialState: {
    auth: false,
    role:undefined,
    name:undefined,
    error : undefined,
    registered:undefined,
  },
  reducers: {
    login: (state,action) => {

      let data = action.payload;
      console.log(data);

      state.auth =true;
      state.error = undefined;
      state.name = data.name;
      state.role = data.role;
    },
    logout: (state,action) => {
        state.auth = false;
        state.error = undefined;
        state.role = undefined;
        state.name = undefined;

    },

    
    // register:   (state, action) => {
    //     state.registered = undefined;
    //     state.error = undefined;
    //     let data = action.payload;

    //     // let res = await registerHook(data);
    //     // console.log("provider",res);
    //     // state.error = res.error;
    //     // state.registered = res.registered;

    //     // console.log("provider state",state.error)
        
    // },

}
});


export const { login, logout } = AuthProvider.actions;

export default AuthProvider.reducer;
