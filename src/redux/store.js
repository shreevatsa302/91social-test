import {
    configureStore,
    combineReducers,
    getDefaultMiddleware
  } from "@reduxjs/toolkit";
  import counterSlice from "./CounterSlice";

  import AuthProvider from "./AuthProvider";
  
  const reducer = combineReducers({
    counter: counterSlice,
    auth: AuthProvider
  });
  
  const store = configureStore({
    reducer,
    middleware: [...getDefaultMiddleware({ thunk: false })]
  });


  
  export default store;
  