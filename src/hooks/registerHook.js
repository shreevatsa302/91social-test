import axios from "axios";

const registerHook = async (data) => {


    let res = await axios.get('http://localhost:8000/users?email='+data.email).then(resp=>{

    console.log("response",resp.data)
        if(resp.data.length>0){
            console.log(true)
            return true;
        }
        else{
            return false;
        }
    }).catch( er=>{
        return false;
    });

    if(!res){
        return await axios.post('http://localhost:8000/users', {
        
            name: data.name,
            password: data.password,
            role:data.role,
            email:data.email
        }).then(resp => {
            console.log(resp);
            return {
                error : undefined,
                registered:true
            };
           
        }).catch(error => {
            console.log(error);
            return {
                error : "SOMETHING WENT WRONG",
                registered:false
            };
        });    
    }
    else{
        return {
            error : "EMAIL ALREADY TAKEN",
            registered:false
        };
    }

   
}

export default registerHook;